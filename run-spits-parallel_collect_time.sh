#!/bin/bash

############################################
# This is the OpenMP run for the RayTracer #
############################################

# You can either pass scenes individually as arguments or 
# leave it blank to run all scenes in the 'scenes' folder
SCENES="${@:1}"
SCENES="${SCENES:-`ls scenes/*.scn`}"

export REPETITIONS=1

declare -a TASK_SZS=(1 10 50)
# declare -a TASK_SZS=(1 10 50 100 150 200)
declare -a FAIL_RATES=(30 50)
# declare -a FAIL_RATES=(0 5 10 15 20 25 30 40 50)

# Ray tracing parameters
export SUPER_SAMPLES=1
export DEPTH_COMPLEXITY=5

# Runtime directory
export PROGDIR="./runtime"
export PROGRAM="$PROGDIR/spitz-run.sh"

# Input binary
export MODULEDIR="./bin"
export MODULE="$MODULEDIR/RayTracer_spits.so"

# Output directory
export OUTDIR="./results/spits-parallel"

# Execution directory
export RUNDIR="./run"

# Ensure the output directory exists
mkdir -p $OUTDIR

# Ensure the execution directory exists
mkdir -p $RUNDIR

# Some PYPITS flags
#
# --overfill is the number of extra tasks sent to each Task Manager 
#            to make better use of the TCP connection, usually a 
#            value a few times larger than the number of workers 
#            works best, this also avoids starvation problems
#
export PPFLAGS="--overfill=20"

# Iterate through selected scenes and 
# run the ray tracer using time 

for repetition in `seq $REPETITIONS`; do
    for scene in $SCENES; do
        for tasksz in "${TASK_SZS[@]}"; do
            for failrate in "${FAIL_RATES[@]}"; do
                FILENAME="$OUTDIR/`basename $scene`"
                FILENAME="${FILENAME%.*}"
                OUTFILE="${FILENAME}.${tasksz}.${failrate}.${repetition}.tga"
                OUTLOG="${FILENAME}.${tasksz}.${failrate}.${repetition}.log"
                OUTTIME="${FILENAME}.${tasksz}.${failrate}.${repetition}.time"
                RUNSCNDIR="$RUNDIR/`basename $scene`"
                echo $scene...
                mkdir -p $RUNSCNDIR
                pushd $RUNSCNDIR
                rm -rf nodes* jm.* log
                (time ../../$PROGRAM $PPFLAGS ../../$MODULE \
                ../../$scene $SUPER_SAMPLES $DEPTH_COMPLEXITY ../../$OUTFILE ${tasksz} ${failrate} ) 2>> ../../${OUTTIME} | tee -a ../../$OUTLOG
                popd
            done
        done
    done
done
