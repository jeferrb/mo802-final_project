for i in *.10.1.log; do
	echo $i
	grep "committed" $i | wc -l
	grep "bad job" $i  | wc -l
done > count.txt


find . -name "ballsInAPit.*.time" -exec sh -c "echo {}; cat {}" \;


for i in *.time; do
	echo -n "$i: "
	# grep $i
	echo -n $(( 60 * $(echo $(cat $i | grep real | cut -d\	 -f2) | cut -dm -f1) + $(echo $(cat $i | grep real | cut -d\	 -f2) | cut -dm -f2 | cut -d. -f1) )) ; echo ".$(cat $i | grep real | cut -d\	 -f2 | cut -d. -f2 | cut -ds -f1)"
done > times.txt
subl times.txt



# -------------


# rm -f *_2_no_delay_parcial/*
rm -rf general_2_no_delay_parcial
rm -rf compute_2_no_delay_parcial
rm -rf memory_2_no_delay_parcial

mkdir general_2_no_delay_parcial
mkdir compute_2_no_delay_parcial
mkdir memory_2_no_delay_parcial

scp username@mympi23679dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' general_2_no_delay_parcial &
scp username@mympi22481dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' compute_2_no_delay_parcial &
scp username@mympi2104dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' memory_2_no_delay_parcial &
scp username@mympi5040dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' memory_2_no_delay_parcial &
scp username@mympi23909dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' compute_2_no_delay_parcial &
scp username@mympi29186dnsprefix1.eastus.cloudapp.azure.com:'/home/username/mo802-final_project/results/spits-parallel/*.time' general_2_no_delay_parcial &

wait

for i in *_2_no_delay_parcial/*.time; do
	if [[ -s $i ]]; then
		echo -n "$i: "
		# grep $i
		echo -n $(( 60 * $(echo $(cat $i | grep real | cut -d\	 -f2) | cut -dm -f1) + $(echo $(cat $i | grep real | cut -d\	 -f2) | cut -dm -f2 | cut -d. -f1) )) ; echo ".$(cat $i | grep real | cut -d\	 -f2 | cut -d. -f2 | cut -ds -f1)"
	fi
done | sort > times.txt
subl times.txt
