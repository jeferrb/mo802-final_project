East US


General purpose:
# 4GB / core
45	"Standard_D16_v3",				"16",			"65636"
55	"Standard_D32_v3",				"32",			"131072"


Memory optimized:
# 8GB / core
98	"Standard_E16_v3",				"16",			"131072"
102	"Standard_E32_v3",				"32",			"262144"


Compute optimized:
# 2 GB / core
116	"Standard_F16s",				"16",			"32768"



High performance:
# 7GB / core
124	"Standard_H16",					"16",			"114688"




AZURE_MACHINES=102
NUMBER_INSTANCES=1
./scripts/run_mpi_benchmark_v8.sh "pass${RANDOM}lala" gGEn7CeoUxlkf/EY6sUlrZFg4ebJw3ZkjJ0QvZ5viW0ES+bRDllVwLQy17M9PcWaM4PoRGhqycd9BFE7OadAqg== ${AZURE_MACHINES} ${NUMBER_INSTANCES} 2>&1 


