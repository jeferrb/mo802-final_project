/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Caian Benedicto <caian@ggaunicamp.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy 
 * of this software and associated documentation files (the "Software"), to 
 * deal in the Software without restriction, including without limitation the 
 * rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in 
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 */


// This define enables the C++ wrapping code around the C API, it must be used
// only once per C++ module.
#define SPITZ_ENTRY_POINT

// #ifndef TASK_SZ
// #define TASK_SZ 3
// #endif
#define MAX_REFLECTIONS 10
#define WIDTH 1920
// #define WIDTH 192
#define HEIGHT 1080
// #define HEIGHT 108

// Spitz serial debug enables a Spitz-compliant main function to allow 
// the module to run as an executable for testing purposes.
// #define SPITZ_SERIAL_DEBUG

#include "Camera.h"
#include "Image.h"
#include "RayTracer.h"
#include "Vector.h"
#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <spitz/spitz.hpp>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/time.h>
#include <stdio.h>


using namespace std;

// Parameters should not be stored inside global variables because the 
// Spitz interface does not guarantee memory isolation between job 
// manager, committer and workers. This can lead to a race condition when 
// each thread tries to initialize the parameters from argc and argv.
struct parameters
{
    std::string who;

    int task_sz;
    int failRate;
    double step;
    int maxReflections;
    int superSamples;
    int depthComplexity;
    // string outFile;
    const char* inFile;
    ifstream inFileStream;
    uint64_t raysCast;
    RayTracer *rayTracer;

    // args :
    // 5 - task size
    // 6 - failure rate
    
    parameters(int argc, const char *argv[], const std::string& who = "") : who(who)
    {
        raysCast = 0;
        std::cout << "[PARAM] Creating parameters.\n";
        if (argc < 7) {
            cerr << "Usage: " << argv[0] << " sceneFile superSamples " <<
            "depthComplexity outFile taskSize failRate" << endl;
            exit(EXIT_FAILURE);
        }
        srand((unsigned)time(0));
        maxReflections = MAX_REFLECTIONS;
        superSamples = atoi(argv[2]);
        depthComplexity = atoi(argv[3]);
        task_sz = atoi(argv[5]);
        failRate = atoi(argv[6]);
        // outFile = argv[4];
        inFile = argv[1];
        inFileStream.open(inFile, ifstream::in);
        
        if (inFileStream.fail()) {
            cerr << "Failed opening file" << endl;
            exit(EXIT_FAILURE);
        }

        rayTracer = new RayTracer(WIDTH, HEIGHT, maxReflections, superSamples, depthComplexity);

        rayTracer->readScene(inFileStream);
        std::cout << who << "readScene " << inFile << std::endl;
        inFileStream.close();

        rayTracer->initTraceRays();
        
    }

    void print()
    {
        std::cout << who << "I'm a parameter!\n";
    }
};

struct simple_parameters
{
    std::string who;

    int task_sz;
    int failRate;

    // args :
    // 5 - task size
    // 6 - failure rate
    
    simple_parameters(int argc, const char *argv[], const std::string& who = "") : who(who)
    {
        std::cout << "[PARAM] Creating parameters.\n";
        if (argc < 7) {
            cerr << "Usage: " << argv[0] << " sceneFile superSamples " <<
            "depthComplexity outFile taskSize failRate" << endl;
            exit(EXIT_FAILURE);
        }
        task_sz = atoi(argv[5]);
        failRate = atoi(argv[6]);
    }

    void print()
    {
        std::cout << who << "I'm a simple parameter!\n";
    }
};

// This class creates tasks.
class job_manager : public spitz::job_manager
{
private:
    int index;
    simple_parameters my_param;

public:
    job_manager(int argc, const char *argv[], spitz::istream& jobinfo) : index(0), my_param(argc, argv, "[JM] ")
    {
        // std::cout << "[JM] Creating Job Manager.\n";
        // my_param.print();
        std::cout << "[JM] Job Manager created.\n";
    }
    
    bool next_task(const spitz::pusher& task)
    {
        if (index <= WIDTH)
        {
            spitz::ostream o;
            
            // Push the current step
            o << index;

            // Serialize the task into a binary stream
            // ...

            {
                printf("[JM] Generated task for i = %d.\n", index);
            }
            // std::cout << "[JM] Generated task for i = " << index << string(".\n");

            // Send the task to the Spitz runtime
            task.push(o);

            // Increment the step by the amount to be packed for each worker
            index += my_param.task_sz;

            // Return true until finished creating tasks
            return true;
        }
        // std::cout << "[JM] Finish task generation\n";
        return false;
    }
    
    ~job_manager()
    {
    }
};

// This class executes tasks, preferably without modifying its internal state
// because this can lead to a break of idempotence between tasks. The 'run'
// method will not impose a 'const' behavior to allow libraries that rely 
// on changing its internal state, for instance, OpenCL (see clpi example).
class worker : public spitz::worker
{
private:
    parameters my_param;
public:
    worker(int argc, const char *argv[]) : my_param(argc, argv, "[WK] ")
    {
        // my_param.print();
        std::cout << "[WK] Worker created.\n";
        /* Intializes random number generator */
        time_t t;
        srand((unsigned) time(&t));
    }

    int run(spitz::istream& task, const spitz::pusher& result)
    {
        // Binary stream used to store the output
        struct timeval start_time;
        gettimeofday(&start_time, NULL);
        spitz::ostream o;
        
        // Deserialize the task, process it and serialize the result
        // Read the current step from the task
        int index;
        task >> index;
        {
            printf("[WK] Processing index %d.\n", index);
        }
        // std::cout << "[WK] Processing index " << index << string(".\n");
        
        // Get the number of steps to compute
        int last_width = min(WIDTH, index + my_param.task_sz);

        // this job will fail?
        int failurePoint = -1;
        int rand_num = (rand()%100);
        bool will_fail = (rand_num < my_param.failRate);
        if (will_fail)
        {   // Where?
            failurePoint = (rand() % ((my_param.task_sz * HEIGHT) + 1));
            {
                printf("[FAIL] The worker %d will fail at %d.\n", index, failurePoint);
            }
            // cout << endl << "[FAIL] The worker " << index <<  " will fail at " << failurePoint << "!" << endl << endl;
        }

        // pop the index
        o.write_data(&index, sizeof(int));

        int currentPoint = 0;

        // Compute each term of the pi expansion
        for (int x = index; x < last_width; x++) {
            // Color my_color;
            // Color my_color(0.0, 0.0005 *x, 0.0);
            for (int y = 0; y < HEIGHT; y++) {
                // Color my_color(0.0, 0.0009 *y, 0.0);
                // Compute each pixel

                if (currentPoint++ == failurePoint)
                {
                    {
                        printf("\n[FAIL] ************ This is a bad job! %d.\n", index);
                    }
                    // cout << endl << "[FAIL] ************ This is a bad job! " << index << endl << endl;
                    // exit(EXIT_FAILURE);
                    // kill(getpid(), SIGKILL);
                    return (1);
                }

                Color my_color = my_param.rayTracer->castRayForPixel(x, y, my_param.raysCast);
                
                // std::cout << "[WK] Processing my_param.raysCast " << my_param.raysCast << string(".\n");
                // Add it to the data stream        
                o.write_data(&my_color, sizeof(Color));
            }
        }

        // cout << "\rDone!" << endl;
        {
            printf("\n[WK] Committed index %d.\n", index);
        }
        // std::cout << "[WK] Committed index " << index << string(".\n");

        // Send the result to the Spitz runtime
        result.push(o);
        struct timeval end_time;
        gettimeofday(&end_time, NULL);
        
        printf("[WK] Time = %f\n", end_time.tv_sec  - start_time.tv_sec + (end_time.tv_usec - start_time.tv_usec)/1000000.0f);
        {
            printf("\n[WK] Processed  index %d.\n", index);
        }
        // std::cout << "[WK] Processed  index " << index << string(".\n");
        return 0;
    }
};

// This class is responsible for merging the result of each individual task 
// and, if necessary, to produce the final result after all of the task 
// results have been received.
class committer : public spitz::committer
{
private:
    Image image;
    string outFile;
    simple_parameters my_param;
public:
    committer(int argc, const char *argv[], spitz::istream& jobinfo) :
    image(WIDTH, HEIGHT), outFile(argv[4]), my_param(argc, argv, "[CO] ")
    {
        std::cout << "[CO] Committer created.\n";
    }
    
    int commit_task(spitz::istream& result)
    {
        // Deserialize the result from the task and use it 
        // to compose the final result

        struct timeval start_time;
        gettimeofday(&start_time, NULL);

        // Accumulate each term of the expansion
        if(result.has_data()) {
            int index;
            // Read the color
            Color my_color;
            result.read_data(&index, sizeof(int));
            int last_width = min(my_param.task_sz, WIDTH - index);
            for (int i = 0; i < last_width; ++i)
            {
                for (int y = 0; y < HEIGHT; y++) {
                    result.read_data(&my_color, sizeof(Color));
                    image.pixel(index+i, y, my_color);
                }
            }
            {
                printf("\n[CO] Result %d committed.\n", index);
            }
            // std::cout << "[CO] Result << " << index << " committed.\n";
        }


        struct timeval end_time;
        gettimeofday(&end_time, NULL);
        
        printf("[CO] Time = %f\n", end_time.tv_sec  - start_time.tv_sec + (end_time.tv_usec - start_time.tv_usec)/1000000.0f);
        return 0;
    }
    
    // Optional. If the result depends on receiving all of the task 
    // results, or if the final result must be serialized to the 
    // Spitz Main, then an additional Commit Job is called.

    int commit_job(const spitz::pusher& final_result) 
    {
        // Process the final result

        std::cout << "[CO] Everybody finished!" << std::endl \
            << "Writing image " << outFile << std::endl;
        
        image.WriteTga(outFile.c_str(), false);

        std::cout << "[CO] The final result is:" << outFile << std::endl;
        // A result must be pushed even if the final result is not passed on
        final_result.push(NULL, 0);
        return 0;
    }

    ~committer()
    {

    }
};

// The factory binds the user code with the Spitz C++ wrapper code.
class factory : public spitz::factory
{
public:
    spitz::job_manager *create_job_manager(int argc, const char *argv[],
        spitz::istream& jobinfo)
    {
        return new job_manager(argc, argv, jobinfo);
    }
    
    spitz::worker *create_worker(int argc, const char *argv[])
    {
        return new worker(argc, argv);
    }
    
    spitz::committer *create_committer(int argc, const char *argv[], 
        spitz::istream& jobinfo)
    {
        return new committer(argc, argv, jobinfo);
    }
};

// Creates a factory class.
spitz::factory *spitz_factory = new factory();
